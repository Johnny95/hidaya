package Base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import Pages.GenderPage;


import java.awt.*;

import static Utilities.Screenshot.takeScreenShot;

public class BaseClass {

    public WebDriver driver;
    protected GenderPage genderPage;

    @BeforeClass
    public void setUP(){
        System.setProperty("webdriver.chrome.driver", "C:\\\\thechromedriver.exe");
        driver= new ChromeDriver();
        driver.manage().window().maximize();
        genderPage = new GenderPage(driver);
        goHome();

    }
    private void goHome(){
        driver.get("https://hidayaacademy.com/prijave/skola-kurana/10?ref=lms&mode=preview");
    }
    @AfterClass
    public void closeAll(){
        driver.quit();
    }
    @AfterMethod
    private void tearDown(ITestResult result) throws AWTException {
        if(ITestResult.FAILURE==result.getStatus())
        {
            takeScreenShot();
        }
    }
}

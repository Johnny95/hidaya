package ApplicationTest;

import Base.BaseClass;
import Pages.PersonalInfoPage;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.concurrent.ThreadLocalRandom;



public class GenderTest extends BaseClass {
    private final String expectedMessage = "Validacija unesenih podataka! Molimo vas odaberite vaš spol.";
    private final String stepsIncluded = "1ODABIR SPOLA\n" + "2LIČNE INFORMACIJE\n" + "3DODATNE INFORMACIJE\n" + "4PREGLED PODATAKA";
    /*
    The test verifies that customer is not able to proceed to a next step without selecting a value
     */
    @Test (priority=2)
    public void verifyUserMustSelectValue()  {
        genderPage.clickOnNext();
        Assert.assertEquals(expectedMessage,genderPage.getWarningText(), "The test case has failed");

    }
    /*
    The test verifies the steps included in the form
     */
    @Test(priority=1)
    public void verifySteps(){
      Assert.assertEquals(stepsIncluded, genderPage.getStepTexts(), "Your test has failed");
    }
    /*
    The test verifies that user is able to select a gender
     */
    @Test(priority=3)
    public void selectGender() throws InterruptedException {
        int randomNum = ThreadLocalRandom.current().nextInt(1, 2 + 1);
        randomNum-=1;
        genderPage.radioButtonCheck(randomNum);
        Assert.assertEquals(genderPage.getRadioButtonStatus(randomNum),"true", "Your test has failed");
        PersonalInfoPage personalInfoPage = new PersonalInfoPage(driver);
        genderPage.clickOnNext();
        Assert.assertEquals(personalInfoPage.getPageTitle(), "Unesite lične informacije", "Your title is not correct");
     }

}

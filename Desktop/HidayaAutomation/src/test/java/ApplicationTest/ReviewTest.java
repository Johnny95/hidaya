package ApplicationTest;

import Base.BaseClass;
import Pages.AdditionalInfoPage;
import Pages.PersonalInfoPage;
import Pages.ReviewPage;
import Utilities.Data;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class ReviewTest extends BaseClass {
    PersonalInfoPage personalInfoPage;
    AdditionalInfoPage additionalInfoPage;
    ReviewPage reviewPage;
    int randomNum;
    int randomNum2;

    @Test(priority = 1)
    public void verifyDataSavedCorrectly() throws ParseException, InterruptedException, AWTException {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        List<String> listInfo = new ArrayList<>(10);
        listInfo = Data.getCorrectData();
        personalInfoPage.populateFields(listInfo);
        personalInfoPage.clickOnNext();
        additionalInfoPage = new AdditionalInfoPage(driver);
        randomNum2 = ThreadLocalRandom.current().nextInt(1, 3 + 1);
        additionalInfoPage.selectEducation(randomNum2);
        String occupation = Data.getOccupation();
        additionalInfoPage.enterOccupation(occupation);
        additionalInfoPage.selectYesForAdditionalFields();
        String company = Data.getCompany();
        additionalInfoPage.enterCompany(company);
        String position = Data.getPosition();
        additionalInfoPage.enterPosition(position);
        String whereHow = Data.whereHow();
        additionalInfoPage.enterSimilarProgram(whereHow);
        additionalInfoPage.proceedToFinalPage();

        reviewPage = new ReviewPage(driver);

        if(randomNum==0){
            Assert.assertEquals(reviewPage.getGender(), "Muško" , "Your gender is not correct");
        } else{
            Assert.assertEquals(reviewPage.getGender(), "Žensko", "Your gender is not correct");
        }
        String name = listInfo.get(0) + " (" + listInfo.get(1) + ") " + listInfo.get(2);
        Assert.assertEquals(reviewPage.getName(),name, "Your name is not correctly saved");

        String address = listInfo.get(7) + "\n" + listInfo.get(8) + " " + listInfo.get(6);
        Assert.assertEquals(reviewPage.getAddress(), address , "Your address is not correct");

        Assert.assertEquals(reviewPage.getEmail(), listInfo.get(3), "Your email is not correct");

        String date = listInfo.get(4);
        StringBuilder newDate = new StringBuilder(date);
        newDate.setCharAt(2, '.');
        newDate.setCharAt(5, '.');

        Assert.assertEquals(reviewPage.getDate().trim(), newDate.toString().trim(), "Your date is not correct");

        String phoneActual = reviewPage.getPhone();
        String phoneExpected = listInfo.get(9);
        phoneExpected = phoneExpected.replaceAll("[\\-\\+\\.\\^:,]","").replaceAll("\\s+","");
        phoneActual = phoneActual.replaceAll("[\\-\\+\\.\\^:,]","").replaceAll("\\s+","");


        Assert.assertEquals(phoneActual.trim(), phoneExpected.trim(), "Your phone is not correct");

        if(randomNum2==1){
            Assert.assertEquals(reviewPage.getEducation(), "Osnovna", "Your education is not correct");
        } if (randomNum2==2){
            Assert.assertEquals(reviewPage.getEducation(), "Srednja (SSS)", "Your education is not correct");
        } if(randomNum2==3){
            Assert.assertEquals(reviewPage.getEducation(), "Visoka (VSS)", "Your education is not correct");
        }

        Assert.assertEquals(reviewPage.getOccupation(), occupation, "Your occupation is not correct");

        Assert.assertEquals(reviewPage.getCompany(), company, "Your occupation is not correct");

        Assert.assertEquals(reviewPage.getPosition(), position, "Your position is not correct");

        Assert.assertEquals(reviewPage.getSimilarProgram(), "Da, " + whereHow, "Your similar program info is not correct");

        driver.navigate().refresh();
    }
    @Test(priority = 2)
    public void verifyBackButton() throws ParseException, InterruptedException, AWTException {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        List<String> listInfo = new ArrayList<>(10);
        listInfo = Data.getCorrectData();
        personalInfoPage.populateFields(listInfo);
        personalInfoPage.clickOnNext();
        additionalInfoPage = new AdditionalInfoPage(driver);
        randomNum2 = ThreadLocalRandom.current().nextInt(1, 3 + 1);
        additionalInfoPage.selectEducation(randomNum2);
        String occupation = Data.getOccupation();
        additionalInfoPage.enterOccupation(occupation);
        additionalInfoPage.selectYesForAdditionalFields();
        String company = Data.getCompany();
        additionalInfoPage.enterCompany(company);
        String position = Data.getPosition();
        additionalInfoPage.enterPosition(position);
        String whereHow = Data.whereHow();
        additionalInfoPage.enterSimilarProgram(whereHow);
        additionalInfoPage.proceedToFinalPage();

        reviewPage = new ReviewPage(driver);
        reviewPage.clickBack();
        Assert.assertEquals(additionalInfoPage.getPageHeadline(), "Unesite dodatne informacije", "Your page title is not correct");
    }

    protected void proceedToPersonalInfo(){
        randomNum = ThreadLocalRandom.current().nextInt(1, 2 + 1);
        randomNum-=1;
        genderPage.radioButtonCheck(randomNum);
        genderPage.proceedToPersonalInfo();
    }
}

package ApplicationTest;

import Base.BaseClass;
import Pages.AdditionalInfoPage;
import Pages.PersonalInfoPage;
import Pages.ReviewPage;
import Utilities.Data;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.ParseException;
import java.util.concurrent.ThreadLocalRandom;

public class AdditionalInfoTest extends BaseClass {
    PersonalInfoPage personalInfoPage;
    AdditionalInfoPage additionalInfoPage;

    @Test (priority = 1)
    public void testUserNotAbleToSelectWithoutEnteringData () throws ParseException, InterruptedException, AWTException {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        personalInfoPage.populateFields(Data.getCorrectData());
        personalInfoPage.clickOnNext();
        additionalInfoPage = new AdditionalInfoPage(driver);
        additionalInfoPage.clickNext();
        Assert.assertEquals(additionalInfoPage.getErrorMessage(), "Validacija unesenih podataka! Molimo vas unesite i odaberite sva ponuđena polja.", "You are able to proceed without selecting mandatory fields");
    }
    @Test (priority = 2)
    public void testAdditionalFieldsAreOpened(){
        additionalInfoPage = new AdditionalInfoPage(driver);
        Assert.assertEquals(additionalInfoPage.checkAdditionalEmploymentFields(), true , "Additional employment fields are not opened");
        Assert.assertEquals(additionalInfoPage.checkSimilarProgramAdditionalField(), true, "Additional similar program fields are not opened");
    }
    @Test (priority = 3)
    public void verifyThatUserIsAbleToProceed() throws InterruptedException {
        additionalInfoPage = new AdditionalInfoPage(driver);
        additionalInfoPage.selectEducation(ThreadLocalRandom.current().nextInt(1, 3 + 1));
        additionalInfoPage.enterOccupation(Data.getOccupation());
        additionalInfoPage.enterCompany(Data.getCompany());
        additionalInfoPage.enterPosition(Data.getPosition());
        additionalInfoPage.enterSimilarProgram(Data.whereHow());
        additionalInfoPage.proceedToFinalPage();
        ReviewPage reviewPage = new ReviewPage(driver);
        Assert.assertEquals(reviewPage.getPageTitle(), "Uspješno ste unijeli sve podatke", "Your title is not correct");
        refresh();
    }
    @Test (priority = 4)
    public void verifyUserIsAbleToProceedWithNoAnswers() throws ParseException, InterruptedException, AWTException {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        personalInfoPage.populateFields(Data.getCorrectData());
        personalInfoPage.clickOnNext();
        additionalInfoPage = new AdditionalInfoPage(driver);
        additionalInfoPage.selectEducation(ThreadLocalRandom.current().nextInt(1, 3 + 1));
        additionalInfoPage.enterOccupation(Data.getOccupation());
        additionalInfoPage.clickNoSimilarProgram();
        additionalInfoPage.clickNotEmployed();
        additionalInfoPage.proceedToFinalPage();
        ReviewPage reviewPage = new ReviewPage(driver);
        Assert.assertEquals(reviewPage.getPageTitle(), "Uspješno ste unijeli sve podatke", "Your title is not correct");
        refresh();
    }
    @Test (priority = 5)
    public void verifyBackButtonAdditionInfoWorks() throws InterruptedException, ParseException, AWTException {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        personalInfoPage.populateFields(Data.getCorrectData());
        personalInfoPage.clickOnNext();
        additionalInfoPage = new AdditionalInfoPage(driver);
        additionalInfoPage.clickBack();
        Assert.assertEquals(personalInfoPage.getPageTitle(), "Unesite lične informacije", "Your title is not correct");
    }

    protected void proceedToPersonalInfo(){
        int randomNum = ThreadLocalRandom.current().nextInt(1, 2 + 1);
        randomNum-=1;
        genderPage.radioButtonCheck(randomNum);
        genderPage.proceedToPersonalInfo();
    }
    private void refresh() throws InterruptedException {
        Thread.sleep(100);
        driver.navigate().refresh();
    }

}

package ApplicationTest;

import Base.BaseClass;
import Pages.AdditionalInfoPage;
import Pages.PersonalInfoPage;

import static Utilities.Data.getCorrectData;
import static Utilities.Data.getIncorrectData;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.awt.*;
import java.text.ParseException;
import java.util.concurrent.ThreadLocalRandom;

public class PersonalInfoTest extends BaseClass {
    PersonalInfoPage personalInfoPage;

    /*
    Test verifies that user is not able to proceed without entering any data, and proper error messages are shown
     */
    @Test(priority = 1)
    public void userNotAbleToProceedWithoutEnteringData() throws InterruptedException {
    proceedToPersonalInfo();
    personalInfoPage = new PersonalInfoPage(driver);
    personalInfoPage.clickOnNext();
    Assert.assertEquals(personalInfoPage.getNumberOfErrors(), 10, "Not All Error Messages Are Shown");
    driver.navigate().refresh();
    }

    /*
    Test verifies that user is not able to proceed with entering incorrect data, and proper error messages are shown.
      Date of birth, country, phone and zip do not give incorrect error messages. I left them out because these text fields are not incorporated.
     */
    @Test(priority = 2)
    public void userNotAbleToProceedIncorrectData() throws AWTException, InterruptedException {
        proceedToPersonalInfo();
        personalInfoPage.populateFields(getIncorrectData());
        personalInfoPage.clickOnNext();
        Assert.assertEquals(personalInfoPage.getNameErrorMessage(), "Dozvoljen je unos od minimalno 3 karaktera.", "Name error message is not correct");
        Assert.assertEquals(personalInfoPage.getFatherNameErrorMessage(), "Dozvoljen je unos od minimalno 3 karaktera.", "Father name error message is not correct");
        Assert.assertEquals(personalInfoPage.getSurnameErrorMessage(), "Dozvoljen je unos od minimalno 3 karaktera.", "Surname name error message is not correct");
        Assert.assertEquals(personalInfoPage.getEmailErrorMessage(), "E-mail nije ispravan.", "Email error message is not correct");
        Assert.assertEquals(personalInfoPage.getCityErrorMessage(), "Dozvoljen je unos od minimalno 2 karaktera.", "City error message is not correct");
        Assert.assertEquals(personalInfoPage.getAddressErrorMessage(), "Dozvoljen je unos od minimalno 3 karaktera.", "City error message is not correct");
        driver.navigate().refresh();
    }
    @Test (priority = 3)
    public void testBackButton() {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        personalInfoPage.clickBack();
        Assert.assertEquals(genderPage.getHeadline(), "Odaberite vaš spol", "You are not taken back to Gender Page");
        driver.navigate().refresh();
    }
    @Test(priority = 4)
    public void testPersonalInfoWithValidData() throws ParseException, AWTException, InterruptedException {
        personalInfoPage = new PersonalInfoPage(driver);
        proceedToPersonalInfo();
        personalInfoPage.populateFields(getCorrectData());
        personalInfoPage.clickOnNext();
        AdditionalInfoPage additionalInfoPage = new AdditionalInfoPage(driver);
        Assert.assertEquals(additionalInfoPage.getPageHeadline(), "Unesite dodatne informacije", "You have not proceeded to the next step.");
    }
    protected void proceedToPersonalInfo(){
        int randomNum = ThreadLocalRandom.current().nextInt(1, 2 + 1);
        randomNum-=1;
        genderPage.radioButtonCheck(randomNum);
        genderPage.proceedToPersonalInfo();
    }
}

package Utilities;

import com.github.javafaker.Faker;
import org.apache.commons.lang3.RandomStringUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;


public class Data {
    public static List<String> getIncorrectData(){
        List<String> data = new ArrayList<>(10);
        for(int i = 0; i<3;i++){
            data.add(i,RandomStringUtils.randomAlphanumeric(2));
        }
        data.add(3,RandomStringUtils.randomAlphanumeric(4) + "@");
        data.add(4, "12.29.2090");
        data.add(5,"0");
        for(int i = 6; i<9; i++){
            data.add(i,RandomStringUtils.randomAlphanumeric(1));
        }
        data.add(9,  String.valueOf(ThreadLocalRandom.current().nextInt(100, 999 + 1)));

        return data;
    }

    public static List<String> getCorrectData() throws ParseException {
        List<String> data = new ArrayList<>(10);
        Faker faker = new Faker();
        data.add(0, faker.name().firstName());
        data.add(1, faker.name().firstName());
        data.add(2, faker.name().lastName());
        data.add(3, faker.internet().emailAddress());
        Random random = new Random();
        int minDay = (int) LocalDate.of(1900, 1, 1).toEpochDay();
        int maxDay = (int) LocalDate.of(2015, 1, 1).toEpochDay();
        long randomDay = minDay + random.nextInt(maxDay - minDay);
        LocalDate randomBirthDate = LocalDate.ofEpochDay(randomDay);
        String s = formatDate(String.valueOf(randomBirthDate));
        data.add(4, s);
        data.add(5, String.valueOf(ThreadLocalRandom.current().nextInt(1, 100 + 1)));
        data.add(6, faker.address().city());
        data.add(7, faker.address().streetAddress());
        data.add(8, faker.address().zipCode());
        data.add(9, phoneNumber());
        return data;
    }
    private static String formatDate(String s) throws ParseException {
        final String OLD_FORMAT = "yyyy-MM-dd";
        final String NEW_FORMAT = "dd-MM-yyyy";

        String oldDateString = s;
        String newDateString;

        SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
        Date d = sdf.parse(oldDateString);
        sdf.applyPattern(NEW_FORMAT);
        return newDateString = sdf.format(d);
    }

    public static String getOccupation(){
        Faker faker = new Faker();
        return faker.company().profession();
    }
    public static String getCompany(){
        Faker faker = new Faker();
        String s = faker.company().name();
        while(s.length()>10){
            s = faker.company().name();
        }
        return s;
    }
    public static String getPosition(){
        Faker faker = new Faker();
        return faker.company().profession();
    }
    public static String whereHow(){
        Faker faker = new Faker();
        return faker.address().streetAddress();
    }
    private static String phoneNumber(){
        Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(10000);

        DecimalFormat df3 = new DecimalFormat("000"); // 3 zeros
        DecimalFormat df4 = new DecimalFormat("0000"); // 4 zeros

        String phoneNumber = df3.format(num1) + "-" + df3.format(num2) + "-" + df4.format(num3);
        return phoneNumber;
    }
}

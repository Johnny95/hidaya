package Utilities;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class Screenshot {
    public static void takeScreenShot() throws AWTException {

        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        try {
            ImageIO.write(image, "png", new File("C:\\Users\\HasanOkanovic\\Desktop\\screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ReviewPage {
    private WebDriver driver;
    private By headlineID = By.cssSelector("#Pregledpodataka6 > div > h3");
    private By genderID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(2) > span.content");
    private By nameID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(3) > span.content");
    private By addressID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(4) > span.content");
    private By emailID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(5) > span.content");
    private By birthDateID= By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(6) > span.content");
    private By stateID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(7) > span.content");
    private By phoneID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(8) > span.content");
    private By educationID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(10) > span.content");
    private By occupationID= By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(11) > span.content");
    private By companyID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(12) > span.content");
    private By positionID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(13) > span.content");
    private By similarProgramID = By.cssSelector("#Pregledpodataka6 > div > div > div:nth-child(14) > span.content");
    private By backButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix.lastStepWizard > div.wizard-footer-left > span > button");
    private By nextButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix.lastStepWizard > div.wizard-footer-right > span > button");

    public ReviewPage(WebDriver driver){
        this.driver=driver;
    }
    public String getPageTitle (){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(headlineID));
        return driver.findElement(headlineID).getText();
    }
    public String getGender() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(genderID));
        return driver.findElement(genderID).getText();
    }
    public String getName (){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(nameID));
        return driver.findElement(nameID).getText();
    }
    public String getAddress () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(addressID));
        return driver.findElement(addressID).getText();
    }
    public String getEmail () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(emailID));
        return driver.findElement(emailID).getText();
    }
    public String getDate (){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(birthDateID));
        return driver.findElement(birthDateID).getText();
    }
    public String getCountry(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(stateID));
        return driver.findElement(stateID).getText();
    }
    public String getPhone() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(phoneID));
        return driver.findElement(phoneID).getText();
    }
    public String getEducation(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(educationID));
        return driver.findElement(educationID).getText();
    }
    public String getOccupation(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(occupationID));
        return driver.findElement(occupationID).getText();
    }
    public String getCompany(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(companyID));
        return driver.findElement(companyID).getText();
    }
    public String getPosition(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(positionID));
        return driver.findElement(positionID).getText();
    }
    public String getSimilarProgram () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(similarProgramID));
        return driver.findElement(similarProgramID).getText();
    }
    public AdditionalInfoPage clickBack(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(backButtonID));
        driver.findElement(backButtonID).click();
        return new AdditionalInfoPage(driver);
    }
}

package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenderPage {
    private WebDriver driver;
    private By nextButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix > div.wizard-footer-right > span > button");
    private By warningTextID = By.cssSelector("#Odabirspola0 > div > div.error-container.alert.alert-dismissable");
    private By stepOneTextID = By.cssSelector("#application_form > div > ul > li.active.checked > a");
    private By stepTwoTextID= By.cssSelector("#application_form > div > ul > li:nth-child(2) > a");
    private By stepThreeTextID= By.cssSelector("#application_form > div > ul > li:nth-child(3) > a");
    private By stepFourTextID= By.cssSelector("#application_form > div > ul > li:nth-child(4) > a");
    private By checkedBoxID= By.cssSelector("input:checked");
    private By radioButtonID1 = By.cssSelector("#Odabirspola0 > div > div > div:nth-child(1)");
    private By radioButtonID2= By.cssSelector("#Odabirspola0 > div > div > div:nth-child(2)");
    private By headlineID = By.cssSelector("#Odabirspola0 > div > h3");
    private WebElement nextButton;

    public GenderPage(WebDriver driver){
        this.driver = driver;
    }
    public void clickOnNext () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(nextButtonID));
        nextButton = driver.findElement(nextButtonID);
        nextButton.click();
    }
    public String getWarningText(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(warningTextID));
        String s = driver.findElement(warningTextID).getText();
        s=s.substring(1);
        s=s.trim();
        return s;
    }
    public String getStepTexts(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(stepOneTextID));
        String s = driver.findElement(stepOneTextID).getText();
        String s2 = s + "\n" + driver.findElement(stepTwoTextID).getText();
        String s3 = s2  + "\n" + driver.findElement(stepThreeTextID).getText();
        String s4 = s3 + "\n" + driver.findElement(stepFourTextID).getText();
        return s4;
    }
    public void radioButtonCheck(int i){
        if(i==0){
            WebDriverWait wait = new WebDriverWait(driver,30);
            wait.until(ExpectedConditions.visibilityOfElementLocated(radioButtonID1));
            driver.findElement(radioButtonID1).click();
        } else if (i==1){
            WebDriverWait wait = new WebDriverWait(driver,30);
            wait.until(ExpectedConditions.visibilityOfElementLocated(radioButtonID2));
            driver.findElement(radioButtonID2).click();
        }else{
            System.out.println("You have not selected an appropriate gender");
        }
    }

    public String getRadioButtonStatus(int i) {
        if(i==0){
            return driver.findElement(By.id("MaleGender")).getAttribute("checked");
        } if (i==1){
            return driver.findElement(By.id("FemaleGender")).getAttribute("checked");
        }else{
             return "You have not selected anything";
        }
    }
    public PersonalInfoPage proceedToPersonalInfo(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(nextButtonID));
        nextButton = driver.findElement(nextButtonID);
        nextButton.click();
        return new PersonalInfoPage(driver);
    }
    public String getHeadline(){
       return driver.findElement(headlineID).getText();
    }
}

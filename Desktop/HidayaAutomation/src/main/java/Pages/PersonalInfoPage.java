package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;
import java.util.List;

public class PersonalInfoPage {
    private WebDriver driver;

    public PersonalInfoPage(WebDriver driver){
        this.driver=driver;
    }
    private By nameID = By.id("FirstName");
    private By fatherNameID = By.id("FatherName");
    private By surnameID = By.id("LastName");
    private By emailID = By.id("Email");
    private By birthDateID = By.id("BirthDate");
    private By countryID = By.id("Country");
    private By cityID = By.id("City");
    private By addressID = By.id("Address");
    private By zipID = By.id("PostalCode");
    private By phoneID = By.id("PhoneNumber");
    private By backButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix > div.wizard-footer-left > span > button");
    private By nextButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix > div.wizard-footer-right > span > button");
    private By errorElementsID = By.xpath("//*[@id=\"Ličneinformacije4\"]/div/div/div");
    private By titlePageID = By.cssSelector("#Ličneinformacije4 > div > h3");
    private By errorNameID= By.cssSelector("#Ličneinformacije4 > div > div > div:nth-child(1) > span");
    private By errorFatherNameID = By.cssSelector("#Ličneinformacije4 > div > div > div:nth-child(2) > span");
    private By errorSurnameID = By.cssSelector("#Ličneinformacije4 > div > div > div:nth-child(3) > span");
    private By errorEmailD = By.cssSelector("#Ličneinformacije4 > div > div > div:nth-child(4) > span");
    private By errorCityID = By.cssSelector("#Ličneinformacije4 > div > div > div:nth-child(7) > span");
    private By errorAddressID = By.cssSelector("#Ličneinformacije4 > div > div > div:nth-child(8) > span");

    public AdditionalInfoPage clickOnNext() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(nextButtonID));
        Thread.sleep(600);
        driver.findElement(nextButtonID).sendKeys(Keys.ENTER);
        return new AdditionalInfoPage(driver);
    }
    public int getNumberOfErrors(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(errorElementsID));
        return driver.findElements(errorElementsID).size();
    }
    public void populateFields(List<String> data) throws AWTException, InterruptedException {
        driver.findElement(nameID).sendKeys(data.get(0));
        driver.findElement(fatherNameID).sendKeys(data.get(1));
        driver.findElement(surnameID).sendKeys(data.get(2));
        driver.findElement(emailID).sendKeys(data.get(3));
        driver.findElement(birthDateID).sendKeys(data.get(4));
        Select drpCountry = new Select(driver.findElement(countryID));
        drpCountry.selectByIndex(Integer.valueOf(data.get(5)));
        driver.findElement(countryID).click();
        driver.findElement(cityID).sendKeys(data.get(6));
        driver.findElement(addressID).sendKeys(data.get(7));
        driver.findElement(zipID).sendKeys(data.get(8));
        driver.findElement(phoneID).sendKeys(data.get(9));
    }

    public GenderPage clickBack () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(backButtonID));
        driver.findElement(backButtonID).click();
        return new GenderPage(driver);
    }
    public String getNameErrorMessage(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(errorNameID));
        return driver.findElement(errorNameID).getText();
    }
    public String getFatherNameErrorMessage(){
        return driver.findElement(errorFatherNameID).getText();
    }
    public String getSurnameErrorMessage(){
        return driver.findElement(errorSurnameID).getText();
    }
    public String getEmailErrorMessage(){
        return driver.findElement(errorEmailD).getText();
    }
    public String getCityErrorMessage(){
        return driver.findElement(errorCityID).getText();
    }
    public String getAddressErrorMessage(){
        return driver.findElement(errorAddressID).getText();
    }
    public String getPageTitle(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(titlePageID));
        return driver.findElement(titlePageID).getText();
    }
}

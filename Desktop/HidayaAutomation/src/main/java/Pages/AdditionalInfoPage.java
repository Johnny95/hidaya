 package Pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import Pages.*;


 public class AdditionalInfoPage {
    private WebDriver driver;
    private By pageHeadlineID = By.cssSelector("#Dodatneinformacije5 > div > h3");
    private By primarySchoolID = By.cssSelector("#Dodatneinformacije5 > div > div.radio-items-container > div:nth-child(1)");
    private By secondarySchoolID = By.cssSelector("#Dodatneinformacije5 > div > div.radio-items-container > div:nth-child(2)");
    private By universityID = By.cssSelector("#Dodatneinformacije5 > div > div.radio-items-container > div:nth-child(3)");
    private String educationID= "#Dodatneinformacije5 > div > div.radio-items-container > div:nth-child(";
    private By occupationID = By.id("Occupation");
    private By employmentYESID = By.cssSelector("#Dodatneinformacije5 > div > div.employeeStatusQuestion > div.radio-items-container.small-buttons > div:nth-child(1)");
    private By employmentNOID = By.cssSelector("#Dodatneinformacije5 > div > div.employeeStatusQuestion > div.radio-items-container.small-buttons > div:nth-child(2)");
    private By companyID= By.id("Company");
    private By positionID = By.id("Position");
    private By attendedSimilarYES = By.cssSelector("#Dodatneinformacije5 > div > div.whereStatusQuestion > div.radio-items-container.small-buttons > div:nth-child(1)");
    private By attendedSimilarNO = By.cssSelector("#Dodatneinformacije5 > div > div.whereStatusQuestion > div.radio-items-container.small-buttons > div:nth-child(2)");
    private By attendedSimilarInfo = By.id("AttentedToSimilarProgramInfo");
    private By backButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix > div.wizard-footer-left > span > button");
    private By nextButtonID = By.cssSelector("#application_form > div > div > div.wizard-card-footer.clearfix > div.wizard-footer-right > span > button");
    private By errorMsgID = By.cssSelector("#Dodatneinformacije5 > div > div.error-container.alert.alert-dismissable");


    public  AdditionalInfoPage(WebDriver driver){
        this.driver = driver;
    }

    public String getPageHeadline () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(pageHeadlineID));
        return driver.findElement(pageHeadlineID).getText();
    }
    public String getErrorMessage () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(errorMsgID));
        String s = driver.findElement(errorMsgID).getText();
        s=s.substring(1);
        s=s.trim();
        return s;
    }
    public void clickNext() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(nextButtonID));
        Thread.sleep(500);
        driver.findElement(nextButtonID).click();
    }
    public PersonalInfoPage clickBack() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(backButtonID));
        Thread.sleep(500);
        driver.findElement(backButtonID).click();
        return new PersonalInfoPage(driver);
    }
    public ReviewPage proceedToFinalPage(){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(nextButtonID));
        driver.findElement(nextButtonID).click();
        return new ReviewPage(driver);
    }
    public boolean checkAdditionalEmploymentFields () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(employmentYESID));
        driver.findElement(employmentYESID).click();
        WebDriverWait wait2 = new WebDriverWait(driver,30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(companyID));
        if(driver.findElement(companyID).isDisplayed()){
            if(driver.findElement(positionID).isDisplayed()){
                return true;
            } else return false;
        } else return false;
    }
    public boolean checkSimilarProgramAdditionalField () {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(attendedSimilarYES));
        driver.findElement(attendedSimilarYES).click();
        WebDriverWait wait2 = new WebDriverWait(driver,30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(attendedSimilarInfo));
        if(driver.findElement(attendedSimilarInfo).isDisplayed()){
            return true;
        }else return false;
    }
    public void selectEducation (int i) throws InterruptedException {
        educationID=educationID + String.valueOf(i) + ')';
        Thread.sleep(200);
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(educationID)));
        driver.findElement(By.cssSelector(educationID)).click();
    }
    public void enterOccupation(String s) {
        driver.findElement(occupationID).sendKeys(s);
    }
    public void enterCompany(String s) {
        driver.findElement(companyID).sendKeys(s);
    }
    public void enterPosition (String s){
        driver.findElement(positionID).sendKeys(s);
    }
    public void enterSimilarProgram (String s){
        driver.findElement(attendedSimilarInfo).sendKeys(s);
    }
    public void clickNotEmployed(){
        driver.findElement(employmentNOID).click();
    }
    public void clickNoSimilarProgram(){
        driver.findElement(attendedSimilarNO).click();
    }
    public void selectYesForAdditionalFields (){
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(employmentYESID));
        driver.findElement(employmentYESID).click();
        WebDriverWait wait2 = new WebDriverWait(driver,30);
        wait2.until(ExpectedConditions.visibilityOfElementLocated(attendedSimilarYES));
        driver.findElement(attendedSimilarYES).click();
    }
}
